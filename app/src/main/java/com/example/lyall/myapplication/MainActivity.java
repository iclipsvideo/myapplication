package com.example.lyall.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DialogTitle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {



    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://iclips-realtime.now.sh");
        }
        catch (URISyntaxException e){

            Context context = getApplicationContext();
            CharSequence text = e.getMessage();
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = getApplicationContext();

        final EditText txtEventName = findViewById(R.id.txtEventName);
        final EditText txtEventText = findViewById(R.id.txtEventText);
        final Button btnSendEvent = findViewById(R.id.btnSendEvent);

        btnSendEvent.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String eventName = txtEventName.getText().toString().trim();
                if (TextUtils.isEmpty(eventName)) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Enter a event name!", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                String eventText = txtEventText.getText().toString().trim();
                if (TextUtils.isEmpty(eventText)) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Enter event text!", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                txtEventName.setText("");
                txtEventText.setText("");

                mSocket.emit(eventName, eventText);

            }
        });

        mSocket.on("user added", onUserAdded);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);

        mSocket.connect();

        this.setTitle("Socket Status = CONNECTED");
        setContentView(R.layout.activity_main);

        Toast toast = Toast.makeText(context, "Connection was made!", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.disconnect();
    }

    private Emitter.Listener onUserAdded = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

                runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = (String) args[0];
                    try {
                        Toast toast = Toast.makeText(getApplicationContext(), "From Server: " + data, Toast.LENGTH_SHORT);
                        toast.show();
                    } catch (Exception e){
                        Toast toast = Toast.makeText(getApplicationContext(), "EXCEPTION: " + e.getMessage(), Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    getActionBar().setTitle("Socket Status = Disconnected");
                    setContentView(R.layout.activity_main);
                }
            });
        }
    };
}
